package br.com.silvaesouza.test;

import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.Test;

import br.com.silvaesouza.stream.Stream;
import br.com.silvaesouza.stream.exception.StreamException;
import br.com.silvaesouza.stream.impl.StreamImpl;
import br.com.silvaesouza.stream.util.StreamUtil;

public class StreamTestCase {
	
	private static final Logger LOGGER = Logger.getLogger(StreamTestCase.class.getCanonicalName());
	
	/**
     * Método genérico para auxílio aos testes.
     */
	private char testFirstChar(Stream stream) {

		long time = System.currentTimeMillis();

		char c = 0;
		try {
			c = StreamUtil.firstChar(stream);
			LOGGER.info(String.format(
					"Identificado primeiro caracter único '%s' com tempo de %d ms de execução.", 
					c, (System.currentTimeMillis() - time)));
		} catch (StreamException e) {
			LOGGER.info(String.format(
					"Não encontrado nenhuma vogal, com tempo de %d ms de execução.", (System.currentTimeMillis() - time)));
		}

		return c;
	}

	/**
	 * Buscar caracter do exemplo.
	 */
    @Test
    public void buscarCaracter() {
		char test = testFirstChar(new StreamImpl("aAbBABacfe"));
		Assert.assertTrue('e' == test);
	
    }
    

    /**
	 * Buscar caracter onde esteja na primeira posição.
	 */
    @Test
    public void buscarCaracterPrimeiraPosicao() {
		char test = testFirstChar(new StreamImpl("baABABac"));
		Assert.assertTrue(0 == test);
    }
    
}
