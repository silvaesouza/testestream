package br.com.silvaesouza.stream.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.silvaesouza.stream.Stream;
import br.com.silvaesouza.stream.exception.StreamException;

/**
 * Classe Util que implementa a lógica para encontrar a primeira vogal que não
 * se repete após a primeira consoante
 * 
 * @author silvaesouza
 *
 */
public class StreamUtil {

	public static char firstChar(Stream input) throws StreamException {
		boolean firstConsanant = false;
		List<Character> orderVowels = new ArrayList<Character>();
		Map<Character, Integer> sumVowels = new HashMap<Character, Integer>();
		Integer vowelsHasMany = 0;

		while (input.hasNext()) {
			char next = Character.toLowerCase(input.getNext());
			if (!firstConsanant && isConsanant(next)) {
				firstConsanant = true;
			} else if (firstConsanant && isVowel(next)) {
				if (sumVowels.containsKey(next)) {
					Integer count = sumVowels.get(next) + 1;
					sumVowels.put(next, count);
					if (count == 2) {
						vowelsHasMany++;
						if (vowelsHasMany == 5)
							break;
					}
				} else {
					sumVowels.put(next, 1);
					orderVowels.add(next);
				}
			}
		}

		for (Character character : orderVowels) {
			if (sumVowels.get(character) == 1) {
				return character;
			}
		}

		throw new StreamException();
	}

	public static boolean isConsanant(char input) {
		int value = ("" + input).replaceAll("(?i)[\\saeiou]", "").length();
		return value > 0 ? true : false;
	}

	public static boolean isVowel(char input) {
		int value = ("" + input).replaceAll("(?i)[^aeiou]", "").length();
		return value > 0 ? true : false;
	}

}
