package br.com.silvaesouza.stream;

/**
 * Interface que define Stream
 * @author silvaesouza
 *
 */
public interface Stream {

	public char getNext();
	public boolean hasNext();
	
}
