package br.com.silvaesouza.stream.impl;

import br.com.silvaesouza.stream.Stream;

/**
 * Implementação de Stream
 * @author silvaesouza
 *
 */
public class StreamImpl implements Stream{
	
	String input;
	Integer indice = 0;
	Integer tamanho = 0;
	
	public StreamImpl(String input) {
		this.input = input;
		if (input != null) 
			this.tamanho = input.length();
	}

	public String getInput() {
		return input;
	}

	@Override
	public char getNext() {
		return input.charAt(indice++);
	}

	@Override
	public boolean hasNext() {
		return indice < tamanho;
	}

}
