package br.com.silvaesouza.stream.exception;

/**
 * Classe de exceção para Stream
 * @author silvaesouza
 *
 */
public class StreamException extends Exception{

	public StreamException() {
		super("Não encontrado vogal que se repita após a primeira consoante");
	}
}
