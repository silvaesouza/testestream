package br.com.silvaesouza.main;

import javax.swing.JOptionPane;

import br.com.silvaesouza.stream.impl.StreamImpl;
import br.com.silvaesouza.stream.util.StreamUtil;

/**
 * Classe principal para executar o programa
 * @author silvaesouza
 * 
 */
public class MainApp {

	public static void main(String[] args) {
		String input;
		
		input = JOptionPane.showInputDialog("Informe uma sequencia:");
		try {
			String message = "First char >> " + StreamUtil.firstChar(new StreamImpl(input));
			JOptionPane.showMessageDialog(null, message, "Sucess",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
}
